<?php

/*add js library*/
function add_theme_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
 
    wp_enqueue_script( 'jfontsize',get_stylesheet_directory_uri() . '/custom/js/jfontsize-2.0.js', array ( 'jquery' ), 2.0, true);
    wp_enqueue_script( 'jstorage', get_stylesheet_directory_uri() . '/custom/js/jstorage.js', array ( 'jquery' ), true);
    wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/custom/js/custom.js', array ( 'jquery' ), true);

    if (isset($_GET['pg_tag'])){
        wp_enqueue_script( 'tag-link', get_stylesheet_directory_uri() . '/custom/js/tagLink.js', array ( 'jquery' ), true);
    }
 
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    
    wp_enqueue_style('custom-css', get_stylesheet_directory_uri() .'/custom/css/style.css');    
    
	wp_enqueue_style('slick-css', get_stylesheet_directory_uri() .'/custom/slick/slick.css');
	wp_enqueue_style('slick-css-theme', get_stylesheet_directory_uri() .'/custom/slick/slick-theme.css');
    wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/custom/slick/slick.js', array ( 'jquery' ), true);
    wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/custom/slick/slick.min.js', array ( 'jquery' ), true);
    wp_enqueue_script( 'simplyscroll-js', get_stylesheet_directory_uri() . '/custom/simplyscroll/jquery.simplyscroll.js', array ( 'jquery' ), true);
    wp_enqueue_script( 'simplyscroll-js', get_stylesheet_directory_uri() . '/custom/simplyscroll/jquery.simplyscroll.min.js', array ( 'jquery' ), true);
	wp_enqueue_style('simplyscroll-css-theme', get_stylesheet_directory_uri() .'/custom/simplyscroll/jquery.simplyscroll.css');
    wp_enqueue_style('custom-raven-css', get_stylesheet_directory_uri() .'/custom/css/raven.css');  
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

require_once('include/custom_shortcodes.php');

// add_action( "admin_notices", function() {
//     echo "<div class='updated'>";
//     echo "<p>";
//     echo "To insert the posts into the database, click the button to the right.";
//     echo "<a class='button button-primary' style='margin:0.25em 1em' href='{$_SERVER["REQUEST_URI"]}&insert_doctors'>Insert Doctor Posts</a>";
//     echo "</p>";
//     echo "<p>";
//     echo "To insert the posts into the database, click the button to the right.";
//     echo "<a class='button button-primary' style='margin:0.25em 1em' href='{$_SERVER["REQUEST_URI"]}&insert_articles'>Insert Articles</a>";
//     echo "</p>";
//     echo "</div>";
// });

function import_doctorlist() {
    global $wpdb; // this is how you get access to the database
    if ( ! isset( $_GET["insert_doctors"] ) ) {
        return;
    }
    $fields = array(
        "custom-field" => array("image","position","title","qualification","articles"),
        "custom-post-type" => "doctors"
    );
    $csvfilepath = get_site_url().'/extra_files/csv/import_doctor.csv';
    
	if (($fh = fopen($csvfilepath, "r")) !== FALSE) {
		// join records as array for looping
		$all_rows = array();
        $header = null;
        $skipHeader = true;
		while ($row = fgetcsv($fh, 0)) {
            if($skipHeader){
                $skipHeader = false;
                continue;
            }
            //do import
            //insert post
            $post_id = wp_insert_post(array (
                'post_type' => 'doctors',
                'post_title' => $row[7],
                'post_status' => 'publish',
            ));
            if ($post_id) {
                $tax_terms = get_terms(array(
                    'taxonomy' => 'servicecategories',
                    'hide_empty' => false,
                ) );
                $termsArray = array();
                foreach($tax_terms as $term) {    
                    $termsArray[$term->term_id] = $term->name;
                }
                $key = null;
                if (in_array($row[1], $termsArray)) {
                    $key = array_search($row[1], $termsArray);
                }else{
                    $insertCatId = wp_insert_term( $row[1], 'servicecategories', array( 'slug' => $row[2] ) );
                    if ( ! is_wp_error( $insertCatId ) )
                    {
                        $key = isset( $insertCatId['term_id'] ) ? $insertCatId['term_id'] : 0;
                    }else{
                         var_dump($insertCatId->get_error_message());
                    }
                }
                //insert category meta
                wp_set_object_terms( $post_id, $key, 'servicecategories' );
                update_term_meta( $key, 'location', $row[3]);
                update_term_meta( $key, 'tel', $row[4]);
                update_term_meta( $key, 'consultation_hours', $row[6]);
                update_term_meta( $key, 'priority', $row[5]);
                // insert post meta
                $positionValueArray = explode('/' , $row[9]);
                $positionValues = array();
                foreach ($positionValueArray as $positionValue) {
                    $positionValues[]["name"] = $positionValue;
                }
                update_field('field_5a0ac3380af71', $positionValues, $post_id);

                $titleValueArray = explode('/' , $row[10]);
                $titleValues = array();
                foreach ($titleValueArray as $titleValue) {
                    $titleValues[]["name"] = $titleValue;
                }
                update_field('field_5a093c9c7ce12', $titleValues, $post_id);

                $qualificationValueArray = explode('/' , $row[11]);
                // update_field('qualification', count($qualificationValueArray), $post_id);
                // foreach ($qualificationValueArray as $index => $qualificationValue) {
                //     $key = 'qualification_'.$index.'_name';
                //     update_field($key, $qualificationValue, $post_id);
                // }
                $qualificationValues = array();
                foreach ($qualificationValueArray as $qualificationValue) {
                    $qualificationValues[]["name"] = $qualificationValue;
                }
                update_field('field_5a093ce07ce13', $qualificationValues, $post_id);

                update_field('priority', $row[12], $post_id);
                
                $imagePath = 'doctorimages' . $row[8];
                $imageId = importImage($imagePath, $post_id);
                update_field('image', $imageId, $post_id);
            }
        }
        var_dump("success");
		fclose($fh);
    }
}

add_action( "admin_init", "import_doctorlist" );

function import_articles() {
    global $wpdb; // this is how you get access to the database
    if ( ! isset( $_GET["insert_articles"] ) ) {
        return;
    }
    $fields = array(
        "custom-field" => array("image","position","title","qualification","articles"),
        "custom-post-type" => "doctors"
    );
    $csvfilepath = get_site_url().'/extra_files/csv/article_tc.csv';
    
	if (($fh = fopen($csvfilepath, "r")) !== FALSE) {
		// join records as array for looping
		$all_rows = array();
        $header = null;
        $skipHeader = true;
		while ($row = fgetcsv($fh, 0)) {
            if($skipHeader){
                $skipHeader = false;
                continue;
            }
            //do import
            //insert post
            $post_id = wp_insert_post(array (
                'post_type' => 'medical_articles',
                'post_title' => $row[4],
                'menu_order' => $row[8],
                'post_status' => 'publish',
            ));
            if ($post_id) {
                $tax_terms = get_terms(array(
                    'taxonomy' => 'articlecategories',
                    'hide_empty' => false,
                ) );
                $termsArray = array();
                foreach($tax_terms as $term) {    
                    $termsArray[$term->term_id] = $term->name;
                }
                $key = null;
                if (in_array($row[1], $termsArray)) {
                    $key = array_search($row[1], $termsArray);
                }else{
                    $insertCatId = wp_insert_term( $row[1], 'articlecategories', array( 'slug' => $row[2] ) );
                    if ( ! is_wp_error( $insertCatId ) )
                    {
                        $key = isset( $insertCatId['term_id'] ) ? $insertCatId['term_id'] : 0;
                    }else{
                         var_dump($insertCatId->get_error_message());
                    }
                }
                //insert category meta
                wp_set_object_terms( $post_id, $key, 'articlecategories' );
                update_term_meta( $key, 'priority', $row[3]);
                // insert post meta
                $docName = $row[5];
                $postID = '';
                global $wpdb;
                $myposts = $wpdb->get_results( $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_title LIKE '%s'", '%'. $wpdb->esc_like( $docName ) .'%') );
                foreach ( $myposts as $mypost ) 
                {
                    $post = get_post( $mypost );
                    $postID = $post->ID;
                    break;
                }       
                $array = array((string)$postID);
                $docStr = serialize($array);
                update_field('author', $docStr, $post_id);                
                $imagePath = 'healtharticles' . $row[7];
                $imageId = importImage($imagePath, $post_id);
                update_field('document', $imageId, $post_id);
            }
		}
		fclose($fh);
    }
}

add_action( "admin_init", "import_articles" );

function importImage($imagePath, $postId){
    // $filename should be the path to the file ( upload directory for example ) 
    $filePath = get_site_url().'/extra_files/media/' . $imagePath; 
    // Check the type of tile. We'll use this as the 'post_mime_type'.
    $filetype = wp_check_filetype( basename( $filePath ), null );
    // Get the path to the upload directory.
    $uploadDir = wp_upload_dir();
    // Prepare an array of post data for the attachment.
    $fileTitle = preg_replace( '/\.[^.]+$/', '', basename( $filePath ) );
    $attachment = array(
        'guid'           => $uploadDir['url'] . '/' . basename( $filePath ), 
        'post_mime_type' => $filetype['type'],
        'post_title'     => $fileTitle,
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    // Insert the attachment.
    $attachId = wp_insert_attachment( $attachment, $filePath, $postId);

    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    // Generate the metadata for the attachment, and update the database record.
    if($attachId != 0) {
        $attachData = wp_generate_attachment_metadata( $attachId, $fileUploadsPath );
        wp_update_attachment_metadata( $attachId, $attachData );
    }
    return $attachId;
}


//add_filter('_wp_relative_upload_path', 'zenpress_wp_plugin_convertaithumbs_upload_path');

/*function zenpress_wp_plugin_convertaithumbs_upload_path($old_file){
	$uploads = wp_upload_dir();
	$file_name = basename($old_file);
	$new_file = $uploads[path] . '/' . $file_name;
	if(!file_exists($new_file) and !is_dir($new_file) and !is_dir($old_file)) {
	// copy this bad boy into the new location
	copy($old_file, $new_file);
	}
	// prepend the month and day subdirectory to the file name, and return it up the chain
	$new_url = $uploads[subdir] . '/' . $file_name;
	$new_url = ltrim($new_url, '/'); // get rid of extra / in path
	return $new_url;
}*/

add_filter('frm_setup_new_fields_vars', 'change_dynamic_blank_option', 25, 2);
function change_dynamic_blank_option($values, $field){
    if ( in_array( $field->id, array( 82,84 ) ) ) {//Replace 82,84 with the IDs of your Dynamic fields
        if ( empty( $values['options'] ) ) {
            return $values;
        }
        $values['options'][0] = 'Period';
    }elseif(in_array( $field->id, array( 114,116 ) )){
        if ( empty( $values['options'] ) ) {
            return $values;
        }
        $values['options'][0] = '時段';
    }elseif(in_array( $field->id, array( 98,100 ) )){
        if ( empty( $values['options'] ) ) {
            return $values;
        }
        $values['options'][0] = '时段';
    }
    return $values;
}

add_action('frm_date_field_js', 'start_and_end_dates', 10, 2);
function start_and_end_dates($field_id, $options){
	$days_between = 0;// Change 0 to the number of days that should be between the start and end dates

	if ( $field_id == 'field_i9kyc') {
		echo ',beforeShowDay: function(dateOne){
            var secondDate=$.datepicker.formatDate("dd-MM-yy", new Date())
            if(secondDate==null){
                return[true];
            }
            var modDate=new Date();
            modDate.setDate(modDate.getDate()-' . $days_between . '+2);
            return [(dateOne > modDate)];
        }';
	} else if ( $field_id == 'field_zkz1z') {
		echo ',beforeShowDay: function(dateTwo){var firstDate=$("#field_i9kyc").datepicker("getDate");if(firstDate==null){
            var firstDate=$.datepicker.formatDate("dd-MM-yy", new Date())
            var modDate=new Date();
            modDate.setDate(modDate.getDate()-' . $days_between . '+2);
            return [(dateTwo > modDate)];
        }var modDate=new Date(firstDate);modDate.setDate(modDate.getDate() +  ' . $days_between . '+2);return [(dateTwo > modDate)];}';
	}else if ( $field_id == 'field_i9kyc2') {
		echo ',beforeShowDay: function(dateOne){
            var secondDate=$.datepicker.formatDate("dd-MM-yy", new Date())
            if(secondDate==null){
                return[true];
            }
            var modDate=new Date();
            modDate.setDate(modDate.getDate()-' . $days_between . '+2);
            return [(dateOne > modDate)];
        }';
	} else if ( $field_id == 'field_zkz1z2') {
		echo ',beforeShowDay: function(dateTwo){var firstDate=$("#field_i9kyc2").datepicker("getDate");if(firstDate==null){
            var firstDate=$.datepicker.formatDate("dd-MM-yy", new Date())
            var modDate=new Date();
            modDate.setDate(modDate.getDate()-' . $days_between . '+2);
            return [(dateTwo > modDate)];
        }var modDate=new Date(firstDate);modDate.setDate(modDate.getDate() +  ' . $days_between . '+2);return [(dateTwo > modDate)];}';
	}else  if ( $field_id == 'field_i9kyc3') {
		echo ',beforeShowDay: function(dateOne){
            var secondDate=$.datepicker.formatDate("dd-MM-yy", new Date())
            if(secondDate==null){
                return[true];
            }
            var modDate=new Date();
            modDate.setDate(modDate.getDate()-' . $days_between . '+2);
            return [(dateOne > modDate)];
        }';
	} else if ( $field_id == 'field_zkz1z3') {
		echo ',beforeShowDay: function(dateTwo){var firstDate=$("#field_i9kyc3").datepicker("getDate");if(firstDate==null){
            var firstDate=$.datepicker.formatDate("dd-MM-yy", new Date())
            var modDate=new Date();
            modDate.setDate(modDate.getDate()-' . $days_between . '+2);
            return [(dateTwo > modDate)];
        }var modDate=new Date(firstDate);modDate.setDate(modDate.getDate() +  ' . $days_between . '+2);return [(dateTwo > modDate)];}';
	} else {
        echo ',beforeShowDay: null';
    }     
}

add_action('frm_date_field_js', 'limit_my_date_field');
function limit_my_date_field($field_id){
  if($field_id == 'field_i9kyc' || $field_id == 'field_zkz1z' || $field_id == 'field_i9kyc2' || $field_id == 'field_zkz1z2' || $field_id == 'field_i9kyc3' || $field_id == 'field_zkz1z3'){ //change FIELDKEY to the key of your date field
    echo ',minDate:0,maxDate:+365';
  } else {
    echo ',minDate: null, maxDate: null';	
  }
}