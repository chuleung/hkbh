<?php
/*
** single.php
** mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
** mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/

get_header();

Mk_Static_Files::addAssets('mk_blog');

$blog_style = 'blog-style-'.mk_get_blog_single_style();
$blog_type = 'blog-post-type-'.mk_get_blog_single_type();
$holder_class = $blog_type . ' ' .$blog_style;

wp_reset_postdata();
$currentLang = ICL_LANGUAGE_CODE;
if($currentLang == "zh-hant"){
	$menu_name = '關於我們';
}elseif($currentLang == "zh-hans"){
	$menu_name = '关于我们';
}else{
	$menu_name = 'About Us';
}
?>

<div id="theme-page" class="postPage hasLeftSideBar">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
	<?php
		$parentPageId = get_page_by_title($menu_name)->ID;
		$parentPageTitle = get_the_title($parentPageId);
        ?>
        <div class="bannerContainer fullPageStyle">
			<?php echo do_shortcode(get_post($parentPageId)->post_content); ?>
        </div>
		<div class="menuContainer leftSideBar">
			<div class="menuHeader"><span class="headerTitle"><?php echo $menu_name; ?></span></div>
			<?php
			$menu = wp_nav_menu( array(
				'menu' => $menu_name,
				'depth' => 1,	
				'items_wrap' => '<ul class="singleLayerMenu %2$s">%3$s</ul>'
			));
			?>
		</div>
		<div class="postContainer rightContainer">
			<?php
			$postType = get_post_type_object(get_post_type(get_the_ID()));
			if ($postType) {
				$pageName = esc_html($postType->labels->singular_name);
			}
			$taxName = $postType->taxonomies[0];
			$terms = get_the_terms( get_the_ID(), $taxName);
			foreach($terms as $term){
				$catName = $term->name;
			}
			$titleItem = ' ';
			$titleItem .= '<div class="pageHeader">';
			$titleItem .= '<h3 class="pageTitle">' . esc_html__( $pageName, 'textdomain' ) . '</h3>';
			$titleItem .= '<p class="postBreadCrumb">';
			if($currentLang  == "zh-hant"){
				$titleItem .= '<span class="pageTitle"><a href="' . get_site_url() .'/about-us/press-centre/?lang=zh-hant">' . esc_html__( $pageName, 'textdomain' ) . '</a></span>';
				$titleItem .= '<span class="postCat"><a href="' . get_site_url() .'/about-us/press-centre?yr='.$catName .'&lang=zh-hant">' . $catName . '</a></span>';
			}elseif($currentLang == "zh-hans"){
				$titleItem .= '<span class="pageTitle"><a href="' . get_site_url() .'/about-us/press-centre/?lang=zh-hans">' . esc_html__( $pageName, 'textdomain' ) . '</a></span>';
				$titleItem .= '<span class="postCat"><a href="' . get_site_url() .'/about-us/press-centre?yr='.$catName .'&lang=zh-hans">' . $catName . '</a></span>';
			}else{
				$titleItem .= '<span class="pageTitle"><a href="' . get_site_url() .'/about-us/press-centre/">' . esc_html__( $pageName, 'textdomain' ) . '</a></span>';
				$titleItem .= '<span class="postCat"><a href="' . get_site_url() .'/about-us/press-centre?yr='.$catName .'">' . $catName . '</a></span>';
			}
			$titleItem .= '<span class="postTitle">' . get_the_title() . '</span>';
			$titleItem .= '</p></div>';
			$titleItem .= '<div class="postHeader">';
			$titleItem .= '<span class="postTitle">' . get_the_title() . '</span>';
			$titleItem .= '</div>';
			echo $titleItem;

			wp_reset_postdata();
			mk_get_view('blog/components', 'blog-single-content');
			// mk_build_main_wrapper( mk_get_view('singular', 'wp-single-leftsidebar', true), false,  $holder_class);
			?>
		</div>
	</div>
</div>
<?php if (get_post_type() == 'press_centre'){?>
<script>
jQuery(function(){
	jQuery(".menu_press_centre").addClass("current_page_item current-menu-item");
});
</script>
<?php } ?>
<?php
get_footer();

