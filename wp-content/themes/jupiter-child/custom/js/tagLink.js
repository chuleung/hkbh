var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

jQuery(function(){
	jQuery(".mk-tabs-tab").css("opacity",0);
    jQuery(".mk-tabs-panes").css("opacity",0);
});
jQuery(window).load(function() {
  	jQuery(".mk-tabs-tab").css("opacity",1);
	jQuery(".mk-tabs-panes").css("opacity",1);
	var pg_tag = getUrlParameter('pg_tag');
    setTimeout(function(){
    	jQuery(".mk-tabs-tab:eq("+pg_tag+")").click();
    	jQuery(".mk-tabs-tab:eq("+pg_tag+")").addClass("is-active");
    	jQuery(".mk-tabs-panes .mk-tabs-pane:eq("+pg_tag+")").addClass("is-active");
    },500);
});

