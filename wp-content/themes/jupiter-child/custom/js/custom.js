jQuery(document).ready(function($) {
	var port = window.location.port;
	var url = '';
	if(port){
		url = window.location.origin + window.location.pathname.split('/').slice(0, 2).join('/');
	}else{
		url = window.location.origin;
	}

	var parseQueryString = function() {
			var str = window.location.search;
			var objURL = {};
		
			str.replace(
				new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
				function( $0, $1, $2, $3 ){
					objURL[ $1 ] = $3;
				}
			);
			return objURL;
		}();
	const lang = parseQueryString['lang'] || 'en';
	var port = window.location.port;
	var url = '';
	if(port){
		url = window.location.origin + window.location.pathname.split('/').slice(0, 2).join('/');
	}else{
		url = window.location.origin;
	}
	if(lang.toLowerCase() === 'zh-hant'){
		$('nav.mk-main-navigation').addClass('zhHant-18');
	}else if(lang.toLowerCase() === 'zh-hans'){
		$('nav.mk-main-navigation').addClass('zhHans-18');
	}

	$("p, h3, a, strong, td").jfontsize({
	    btnMinusClasseId: '#jfontsize-m2', // Defines the class or id of the decrease button
	    btnDefaultClasseId: '#jfontsize-d2', // Defines the class or id of default size button
	    btnPlusClasseId: '#jfontsize-p2', // Defines the class or id of the increase button
	    btnMinusMaxHits: 2, // How many times the size can be decreased
	    btnPlusMaxHits: 2, // How many times the size can be increased
		sizeChange: 1 // Defines the range of change in pixels
	});
	$("span, svg, img, path, i").jfontsize({
	    btnMinusClasseId: '#jfontsize-m2', // Defines the class or id of the decrease button
	    btnDefaultClasseId: '#jfontsize-d2', // Defines the class or id of default size button
	    btnPlusClasseId: '#jfontsize-p2', // Defines the class or id of the increase button
	    btnMinusMaxHits: 0, // How many times the size can be decreased
	    btnPlusMaxHits: 0, // How many times the size can be increased
		sizeChange: 1 // Defines the range of change in pixels
	});

	$(".homepageGridContainer").slick({
		dots: true,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		rows: 2,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					rows: 3,
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					rows: 2,
				}
			}
		]
	});
	$(".homepageGridContainer").fadeIn();
	
	$(".floatButton").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(".floatmenuContainer").removeClass("active");
			if($(".floatmenuContainer").hasClass("enVersion")){
				$(".floatmenuContainer").animate({right:"-225px"});
			}else{
				$(".floatmenuContainer").animate({right:"-145px"});
			}
		}else{
			$(".floatmenuContainer").addClass("active");
			$(this).addClass("active");
			$(".floatmenuContainer").animate({right:"0px"});
			$(document).on("mouseup.hideDocClick", function (e){
				var container = $(".floatmenuContainer");	    
				if (!container.is(e.target) && container.has(e.target).length === 0) 
				{
					if(container.hasClass("active")){
						$(".floatButton").removeClass("active");
						container.removeClass("active");
						if(container.hasClass("enVersion")){
							container.animate({right:"-225px"});
						}else{
							container.animate({right:"-145px"});
						}
						$(document).off('.hideDocClick');
					}
				}
			});
		}
	});	
	
	$("#servicesOverviewSideBar .multiLayerMenu .menu-item>a").click(function(e){
		if($(e.target).hasClass("actived")){
			$(e.target).removeClass("actived");
		}else{
			$(e.target).addClass("actived");
		}
		if($(e.target).siblings().size() > 0){
			e.preventDefault();
			if($(e.target).siblings(".sub-menu").css('display') == 'none'){
				$(e.target).parent().find(".sub-menu").show();
			}else{
				$(e.target).parent().find(".sub-menu").hide();
			}
		}
	});

	$(".bottom-corner-btns .js-bottom-corner-btn .mk-svg-icon").remove();
	$(".bottom-corner-btns .js-bottom-corner-btn").append("<img src='" + url + "/wp-content/themes/jupiter-child/custom/images/backToTopIcon.png'>");

	$(".homepageSlider .mk-slider-holder .mk-edge-nav .mk-edge-prev .mk-edge-icon-wrap .mk-svg-icon").remove();
	$(".homepageSlider .mk-slider-holder .mk-edge-nav .mk-edge-prev .mk-edge-icon-wrap").append("<img src='" + url + "/wp-content/themes/jupiter-child/custom/images/bannerLeftArrow.png'>");

	$(".homepageSlider .mk-slider-holder .mk-edge-nav .mk-edge-next .mk-edge-icon-wrap .mk-svg-icon").remove();
	$(".homepageSlider .mk-slider-holder .mk-edge-nav .mk-edge-next .mk-edge-icon-wrap").append("<img src='" + url + "/wp-content/themes/jupiter-child/custom/images/bannerRightArrow.png'>");

	$(".video-container").append('\
		<svg class="videoPlayButton" viewBox="0 0 200 200" alt="Play video">\
			<circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>\
			<polygon points="70, 55 70, 145 145, 100" fill="#fff"/>\
		</svg>\
	');
	$(".video-container > video").removeAttr("autoplay");
	$(".video-container > .videoPlayButton").click(function(){
		var videoPlay = $(this).closest(".video-container").find("video");
		if (videoPlay[0].paused){ 
			videoPlay[0].play(); 
			$(this).addClass("hidden");
		}else{ 
			videoPlay[0].pause();
			$(this).removeClass("hidden");
		}
	});
	$(".video-container > video").click(function(){
		var videoControl= $(this).closest(".video-container").find(".videoPlayButton");
		if ($(this)[0].paused){ 
			$(this)[0].play(); 
			videoControl.addClass("hidden");
		}else{ 
			$(this)[0].pause();
			videoControl.removeClass("hidden");
		}
	});
    $('.mk-tabs-tab').click(function(){
        var oj = $(this);
        setTimeout(function(){
            var pane = oj.parent().parent().find('.mk-tabs-pane.is-active');
            //pane.css('background','red');
            var findRedirectElement = pane.find('.custom_link_hook');
            if (findRedirectElement.length > 0){
                window.location.href = findRedirectElement.attr('tar');
            }
        },500); 
    });
});