<?php
/*
** single.php
** mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
** mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/

get_header();

Mk_Static_Files::addAssets('mk_blog');

$blog_style = 'blog-style-'.mk_get_blog_single_style();
$blog_type = 'blog-post-type-'.mk_get_blog_single_type();
$holder_class = $blog_type . ' ' .$blog_style;

wp_reset_postdata();
$currentLang = ICL_LANGUAGE_CODE;
if($currentLang == "zh-hant"){
	$menu_name = '職位空缺';
}elseif($currentLang == "zh-hans"){
	$menu_name = '职位空缺';
}else{
	$menu_name = 'Job Vacancies';
}
?>

<div id="theme-page" class="postPage fullPageStyle">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
        <?php
        $postType = get_post_type_object(get_post_type(get_the_ID()));
        if ($postType) {
            $pageName = esc_html($postType->labels->singular_name);
        }
		$parentPageId = get_page_by_title($menu_name)->ID;
		$parentPageTitle = get_the_title($parentPageId);
        ?>
        <div class="bannerContainer fullPageStyle">
			<?php echo do_shortcode(get_post($parentPageId)->post_content); ?>
        </div>
		<div class="postContainer">
			<?php
			$titleItem = ' ';
			$titleItem .= '<div class="pageHeader">';
			$titleItem .= '<h3 class="pageTitle">' . $pageName . '</h3>';
			$titleItem .= '<p class="postBreadCrumb">';
			$titleItem .= '<span class="pageTitle"><a href="' . get_site_url() .'/' . $pageName . '/">' . $pageName . '</a></span>';
			$titleItem .= '<span class="postTitle">' . get_the_title() . '</span>';
			$titleItem .= '</p></div>';
			$titleItem .= '<div class="postHeader">';
			$titleItem .= '<span class="postTitle">' . get_the_title() . '</span>';
			$titleItem .= '</div>';
			echo $titleItem;

			wp_reset_postdata();
			mk_get_view('blog/components', 'blog-single-content');
			?>
		</div>
	</div>
</div>

<?php
get_footer();

