<?php

add_shortcode( 'getHomepageGrid', 'get_homepage_grid');
add_shortcode( 'getHomepagePromotion', 'get_homepage_promotion');
add_shortcode( 'getHeaderSubMenu', 'get_header_submenu');
add_shortcode( 'getJobVacancies', 'get_job_vacancies');
add_shortcode( 'getAccreditationsAwards', 'get_accreditations_awards');

function get_homepage_grid( $atts ) {	
	$slugName = $atts['slug'];
	$output = '';
	$output .= '<div class="homepageGridContainer">';
	wp_reset_postdata();
	$args = array(
		'posts_per_page' => -1,
		'order'=> 'ASC',
		'meta_key' => 'priority',
		'post_status' => 'publish',
		'orderby' => 'meta_value_num',
		'post_type' => $slugName
	);
	$loop = new WP_Query($args);
	if( $loop->have_posts() ) {
		while ($loop->have_posts()) : $loop->the_post();
		$current_post_id =  get_field("page_id");
		$postLink = esc_url(get_permalink(get_field("page_id")));
		$defaultImageId = 198;
		$thumbnail = empty(get_the_post_thumbnail()) ?  wp_get_attachment_image( $defaultImageId, array('224', '168'), "", array( "class" => "img-responsive" ) ) : get_the_post_thumbnail();
		//content grid
		$output .= '<div class="gridInner">';
		$output .= '<div class="gridThumbnail"><a href="' . $postLink . '">' . $thumbnail . '</a></div>';
		$output .= '<div class="gridTitle"><a href="' . $postLink . '">' . get_the_title() . '</a></div>';
		$output .= '<div class="gridDescription">'. get_field("description") .'</div>';
		$output .= '<div class="gridLink"><a href="' . $postLink. '">' . esc_html__( 'Read More', 'textdomain' ) . '</a></div>';
		$output .= '</div>';
		endwhile;
	}
	$output .= '</div>';
	wp_reset_postdata();
	return $output;
}

function get_homepage_promotion( $atts ) {	
	$slugName = $atts['slug'];
	$output = '';
	$output .= '<div class="promotionContainer"><ul class="promotionPosts">';
	wp_reset_postdata();
	$args = array(
		'posts_per_page' => -1,
		'order'=> 'ASC',
		'meta_key' => 'priority',
		'orderby' => 'meta_value_num',
		'post_status' => 'publish',
		'post_type' => $slugName
	);
	$loop = new WP_Query($args);
	if( $loop->have_posts() ) {
		while ($loop->have_posts()) : $loop->the_post();
		$output .= '<li class="promotionInner"><a href="' . esc_url(get_permalink(get_the_ID())) . '">' . get_the_title() . '</a></li>';
		endwhile;
	}
	$output .= '</ul></div>';
	wp_reset_postdata();
	$output .= '<script type="text/javascript">
			var rowNum = 5;
			var scrollHeight = 50 * rowNum;
			if(jQuery(".promotionPosts").outerHeight() > scrollHeight){
				jQuery(".promotionContainer").simplyScroll({
					customClass: "vert",
					orientation: "vertical",
					autoMode: "loop"
				});
			}	
		</script>';
	return $output;
}

function get_header_submenu( $atts ) {	
	wp_reset_postdata();
	$slugName = $atts['slug'];
	$menu = wp_nav_menu( array(
		'menu' => $slugName,
		'depth' => 3
	));
	wp_reset_postdata();
}

function get_job_vacancies( $atts ) {	
	$slugName = $atts['slug'];
	$output = '';
	$output .= '<div class="jobVacanciesContainer"><table class="postsTable">';
	$output .= '<tr class="tableHeader">';
	$output .= '<th></th>';
	$output .= '<th><span>' . esc_html__( 'Release Date', 'textdomain' ) . '</span></th>';
	$output .= '<th><span>' . esc_html__( 'Deadline', 'textdomain' ) . '</span></th>';
	$output .= '</tr>';
	wp_reset_postdata();
	$args = array(
		'post_status' => 'publish',
		'orderby' => 'publish_date',
		'order' => 'DESC',
		'post_type' => $slugName
	);
	$posts = get_posts($args);
	$count = '';
	if( $posts ) {
		foreach($posts as $post ){
			$count++;
			$postId = $post->ID;
			$deadLine = get_field("deadline", $postId);
			$deadLine = strtr($deadLine, '/', '-');
			$deadLineTime = strtotime($deadLine);
			$currentTime = strtotime(current_time( 'mysql' ));
			if($currentTime > $deadLineTime){
				continue;
			}			
			$output .= '<tr class="post">';
			$output .= '<td class="postTitle"><a href="' . get_permalink($postId)  . '">' . $count . '. ' . get_the_title($postId) . '</a></td>';
			$output .= '<td class="postReleaseDate">' . date('d/m/Y', strtotime($post->post_date)) . '</td>';
			$deadLine = get_field("deadline", $postId);
			$deadLine = strtr($deadLine, '/', '-');
			$output .= '<td class="postDeadline">' . date('d/m/Y', strtotime($deadLine)) . '</td>';
			$output .= '</tr>';
		}
	}
	$output .= '</table></div>';
	wp_reset_postdata();
	return $output;
}

function get_accreditations_awards( $atts ) {	
	$slugName = $atts['slug'];
	$output = '';
	if($slugName == 'accreditations'){
		$output .= '<div class="accreditationPostsContainer postsContainer">';
	}else{
		$output .= '<div class="awardPostsContainer postsContainer">';
	}
	wp_reset_postdata();
	$args = array(
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => -1,
		'suppress_filters'=>0,
		'post_type' => $slugName
	);
	$posts = get_posts($args);
	if( $posts ) {
		foreach($posts as $post ){
			$postId = $post->ID;
			$output .= '<div class="postContainer">';
			$output .= '<p class="postTitle"><span>' . get_the_title($postId) . '</span></p>';
			$output .= '<p>' . do_shortcode($post->post_content) . '</p>';
			$output .= '</div>';
		}
	}
	$output .= '</div>';
	$output .= '<script type="text/javascript">';
	$output .= 'jQuery(document).ready(function($) {
					$(".postsContainer .postContainer").delay(3000).show(0);
					$(".postsContainer .postContainer .postTitle").unbind().click(function(){
						$(".postsContainer .postContainer").siblings().find(".mk-in-viewport").hide();
						if($(this).hasClass("active")){
							$(".postsContainer .postContainer").siblings().find(".postTitle").removeClass("active");
						}else{
							$(".postsContainer .postContainer").siblings().find(".postTitle").removeClass("active");
							$(this).addClass("active");
							$(this).next(".mk-in-viewport").fadeIn();
						}
					});
					$(".vc_tta-tabs-container .vc_tta-tabs-list .vc_tta-tab").unbind().click(function(){
						var tabString = $(this).find("a").attr("href");
						var hashString = $(location).attr("hash");
						if(tabString != hashString){
							$(".postsContainer .postContainer").siblings().find(".mk-in-viewport").fadeOut();
							$(".postsContainer .postContainer").siblings().find(".postTitle").removeClass("active");
						}
					});
				});';
	$output .= '</script>';
	wp_reset_postdata();
	return $output;
}