<?php /* Template Name: servicesOverview Page */

get_header();


Mk_Static_Files::addAssets('mk_button'); 
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');

wp_reset_postdata();
?>

<div id="theme-page" class="staticPage hasLeftSideBar">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
		<?php
		$currentPageId = get_the_ID();
		if (get_post_type($currentPageId) == "revision"){
			$parentPageId = wp_get_post_parent_id(wp_get_post_parent_id( get_the_ID()));			
		} else {
			$parentPageId = wp_get_post_parent_id( get_the_ID() );			
		}
		$parentPageTitle = get_the_title($parentPageId);
		$parentPageArray = get_post_ancestors( get_the_ID() );
		$ancestorPageId = end($parentPageArray);
		$ancestorPageTitle = get_the_title($ancestorPageId);
        ?>
        <div class="bannerContainer fullPageStyle">
			<?php
			if($parentPageId == $ancestorPageId){

			}else{
				echo do_shortcode(get_post($parentPageId)->post_content);
			}?>
        </div>
		<?php if(!empty($pageFeatureImgUrl)){?>
		<?php }?>
		<div id="servicesOverviewSideBar" class="menuContainer leftSideBar">
            <div class="menuHeader"><span class="headerTitle"><?php echo $ancestorPageTitle; ?></span></div>
			<?php
			$menu = wp_nav_menu( array(
				'menu' => $ancestorPageTitle,
				'depth' => 3,	
				'items_wrap' => '<ul class="multiLayerMenu serviceOverviewMenu %2$s">%3$s</ul>'
			));
			?>
		</div>
		<div class="pageContainer rightContainer">
			<?php
			$titleItem = '<div class="postHeader"><span class="postTitle">' . get_the_title() . '</span></div>';
			echo $titleItem;
			wp_reset_postdata();
			the_content();
			// mk_build_main_wrapper( mk_get_view('singular', 'wp-page', true) );
			?>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		var bannerContaienr = jQuery("#singlePageBanner");
		jQuery(".bannerContainer").append(bannerContaienr.html());
		jQuery(".bannerContainer .vc_hidden-lg").attr('style','display: block !important');
		jQuery("#servicesOverviewSideBar .serviceOverviewMenu .current-page-ancestor>a").addClass("actived");
	})			
</script>
<?php
get_footer();


