<?php /* Template Name: Health Articles Page */

get_header();


Mk_Static_Files::addAssets('mk_button'); 
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');

wp_reset_postdata();
?>

<div id="theme-page" class="staticPage hasLeftSideBar">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
		<?php
		$currentPageId = get_the_ID();
		if (get_post_type($currentPageId) == "revision"){
			$parentPageId = wp_get_post_parent_id(wp_get_post_parent_id( get_the_ID()));			
		} else {
			$parentPageId = wp_get_post_parent_id( get_the_ID() );			
		}
		$parentPageTitle = get_the_title($parentPageId);
        ?>
        <div class="bannerContainer fullPageStyle">
			<?php echo do_shortcode(get_post($parentPageId)->post_content); ?>
        </div>
		<div class="menuContainer leftSideBar">
            <div class="menuHeader"><span class="headerTitle"><?php echo $parentPageTitle; ?></span></div>
			<?php
			$menu = wp_nav_menu( array(
				'menu' => $parentPageTitle,
				'depth' => 1,	
				'items_wrap' => '<ul class="singleLayerMenu %2$s">%3$s</ul>'
			));
			wp_reset_postdata();
			?>
		</div>
		<div class="pageContainer rightContainer">
			<?php
			$titleItem = '<div class="postHeader"><span class="postTitle">' . get_the_title() . '</span></div>';
			echo $titleItem;
			$postType = 'medical_articles';
			$taxName = 'articlecategories';
			$terms = get_terms( $taxName, array(
				'hide_empty' => 0
			));
			$articlesListArr = array();
			foreach($terms as $key => $term){
				$termName = $term->name;
				$termSlug = $term->slug;
				$termId = $term->term_id;
				$termPriority = get_field("priority", "servicecategories_$termId");
				$articlesListArr[$termPriority] = $termName;
			}
			wp_reset_postdata();
			ksort($articlesListArr);
			if(ICL_LANGUAGE_CODE == "en"){
				$listShow = ' ';
			}else{
				$listShow = ' ';
				$listShow .= '<div class="articlesListContainer"><select name="articlesCat" id="articlesCatSelect">';
				$listShow .= '<option class="articlesCat defalut" value="default">' . esc_html__( 'Health Articles', 'textdomain' ) . '</option>';
				foreach($articlesListArr as &$value){
					if ($value === reset($articlesListArr)){
						$listShow .= '<option class="articlesCat" value="' . $value . '">' . $value . '</option>';
					}else{
						$listShow .= '<option class="articlesCat" value="' . $value . '">' . $value . '</option>';
					}
				}
				$listShow .= '</select></div>';
			}
			echo $listShow;
			?>			
            <div class="langAttn">
				<span class="attnContent">
					<?php if(ICL_LANGUAGE_CODE == "en"){ ?>
						<a href="<?php echo get_site_url() ?>/health-information/health-articles/?lang=zh-hant">
						<?php echo esc_html__( 'All articles are in Chinese only.', 'textdomain' );?>
						</a>
					<?php }else{
						echo '';
					}
					?>
				</span>
			</div>
			<table id="articlePosts">
			</table>
		</div>
	</div>
</div>

<?php
		reset($articlesListArr);
		$first_key = key($articlesListArr);
		$args = array(
			'order'=> 'ASC',
			'post_type' => 'medical_articles',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'suppress_filters'=>0
		);
		$posts = get_posts( $args );
		$result = array();
		foreach($posts as $post ){
			$terms = get_the_terms( get_the_ID(), 'articlecategories' );
			$image =  get_field('document')['url'];
			$authorPost =  get_field('doctor_post');
			if($authorPost === NULL || !$authorPost){
				$authorName = get_field('author_name');
				$authorPosition = get_field('author_title');
			}else{
				$authorPostId = $authorPost[0];
				$authorName = (ICL_LANGUAGE_CODE == "en" ? 'Dr.' : ' ') . get_the_title($authorPostId) . (ICL_LANGUAGE_CODE == "en" ? ' ' : esc_html__( 'Doctor', 'textdomain' ));
				$authorPositionArr = get_field('position', $authorPostId);
				$authorPosition = $authorPositionArr[0]['name'];
			}
			foreach($terms as $term){
				$termName = $term->name;
				$result[$termName][] = [
					'title'=> get_the_title(),
					'link'=>$image,
					'author'=>$authorName,
					'authorPosition'=> $authorPosition
				];
			}
		}
	?>

<script type='text/javascript'>
	function buildPost(post){
		var postLink = post.link;
		var postTitle = post.title;
		var postAuthor = post.author;
		var postAuthorPosition = post.authorPosition;
		if(!postAuthorPosition){
			var output = "<tr class='post'><td class='postLink'><a href='"+postLink+"' target='_blank'>" + postTitle + "</a></td><td class='authorInfo'>" + postAuthor + "</td></tr>";
		}else{
			var output = "<tr class='post'><td class='postLink'><a href='"+postLink+"' target='_blank'>" + postTitle + "</a></td><td class='authorInfo'>" + postAuthor + "("+postAuthorPosition+")</td></tr>";
		}
		return output;
	}
	jQuery(document).ready(function($) {
		var posts = <?php echo json_encode($result); ?>;
		var defaultCat = "default";
		$("#articlesCatSelect").val(defaultCat);
		$("#articlesCatSelect").on("change", function() {		
			var value = $(this).val();
			jQuery('#articlePosts').html(
				(posts[value] || []).map(buildPost)
			)
		});
	});
</script>

<?php
get_footer();


