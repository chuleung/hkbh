<?php

/**
 * template part for responsive search. views/header/global
 *
 * @author 		Artbees
 * @package 	jupiter/views
 * @version     5.0.0
 */

global $mk_options;

if(!is_header_show() && $view_params['is_shortcode'] != 'true') return false;

$menu_location = !empty($view_params['menu_location']) ? $view_params['menu_location'] : mk_main_nav_location();

$hide_header_nav = isset($mk_options['hide_header_nav']) ? $mk_options['hide_header_nav'] : 'true';

?>

<div class="mk-responsive-wrap">

	<?php 
		mk_get_header_view('master', 'main-nav');
	// if($hide_header_nav != 'false') { 
	// 	echo wp_nav_menu(array(
	// 	    'theme_location' => $menu_location,
	// 	    'container' => 'nav',
	// 	    'menu_class' => 'mk-responsive-nav',
	// 	    'echo' => false,
	// 	    'fallback_cb' => 'mk_link_to_menu_editor',
	// 	    'walker' => new mk_main_menu_responsive_walker,
	// 	));
	// }
	?>

	<?php 
		if ( defined( 'ICL_SITEPRESS_VERSION' ) && defined( 'ICL_LANGUAGE_CODE' ) ) {
			mk_get_header_view('toolbar', 'wpml-nav');
		}
	?>	

</div>
