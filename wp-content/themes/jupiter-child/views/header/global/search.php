<?php

/**
 * template part for search in header and header toolbar. views/header/global
 *
 * @author 		Artbees
 * @package 	jupiter/views
 * @version     5.0.0
 */

global $mk_options;

if ($mk_options['header_search_location'] != $view_params['location']) return false; ?>

<div class="mk-header-search main-nav-side-search">
    <a class="mk-search-trigger <?php echo $icon_height; ?> mk-toggle-trigger" href="#"><i class="mk-svg-icon-wrapper"><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, 'mk-icon-search', 16); ?></i></a>

	<div id="mk-nav-search-wrapper" class="mk-box-to-trigger">
		<form method="get" id="mk-header-navside-searchform" action="<?php echo home_url('/'); ?>">
			<i class="nav-side-search-icon"><input type="submit" value=""/><?php Mk_SVG_Icons::get_svg_icon_by_class_name(true,'mk-moon-search-3',16); ?></i>
			<input type="text" name="s" id="mk-ajax-search-input" autocomplete="off" placeholder="<?php echo esc_html__( 'Search', 'textdomain' ); ?>" onfocus="if (this.value == 'Search Here') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search Here';}"/>
			<input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
			<?php wp_nonce_field('mk-ajax-search-form', 'security'); ?>
			</form>
		<ul id="mk-nav-search-result" class="ui-autocomplete"></ul>
	</div>
	
	<div class="respFloatContainer">
		<div class="floatButton"><p><?php echo esc_html__( 'Quick Search', 'mk_framework' );?></p></div>
		<?php 
		$menu_name = 'quickSearchMenu';
		if ( isset( $menu_name ) ) {
			$menu = wp_get_nav_menu_object( $menu_name );
			
			$menu_items = wp_get_nav_menu_items($menu->term_id);
			$menu_list = '<div class="floatMenu menuContent"><ul class="menuList">';
			
			foreach ( (array) $menu_items as $key => $menu_item ) {
				$title = $menu_item->title;
				$url = $menu_item->url;
				$menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
			}
			$menu_list .= '</ul></div>';
		} else {
			$menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
		}
		echo $menu_list;
		?>
	</div>
	<div class="clearboth"></div>
</div>
