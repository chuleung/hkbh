<?php

/**
 * template part for header toolbar WPML Language Switcher. views/header/toolbar
 *
 * @author 		Artbees
 * @package 	jupiter/views
 * @version     5.0.0
 */

$languages = icl_get_languages( 'skip_missing=0&orderby=id' );
$output    = "";

if ( is_array( $languages ) ) {
    
    $output.= '<div class="mk-language-nav"><a href="#">'.Mk_SVG_Icons::get_svg_icon_by_class_name(false, 'mk-li-web', 16) . '</a>';
    $output.= '<div class="mk-language-nav-sub-wrapper"><div class="mk-language-nav-sub">';
    $output.= "<ul class='mk-language-navigation'>";
    $langNum = 0;
    foreach ( $languages as $lang ) {
        if( $lang['default_locale']==get_locale() ) {
            continue;
        }
        $langNum++;
        $output.= "<li class='language_" . esc_attr( $lang['language_code'] ) . "'><a href='" . esc_url( $lang['url'] ) . "'>";
        if($lang['language_code'] == "zh-hant"){
            $langShow = "繁體";
            if($langNum == 2){
                $output.= "<span class='mk-lang-name'>" . $langShow . "</span>";
            }else{
                $output.= "<span class='mk-lang-name'>" . $langShow . " /</span>";
            }
        }elseif($lang['language_code'] == "zh-hans"){
            $langShow = "简体";
            if($langNum == 2){
                $output.= "<span class='mk-lang-name'>" . $langShow . "</span>";
            }else{
                $output.= "<span class='mk-lang-name'>" . $langShow . " /</span>";
            }
        }else{
            $langShow = "ENG";
            $output.= "<span class='mk-lang-name'>" . $langShow . "</span>";
        }
        $output.= "</a></li>";
    }
    
    $output.= "</ul></div></div></div>";
}

echo $output;
