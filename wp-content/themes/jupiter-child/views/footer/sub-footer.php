<?php
/**
 * template part for sub footer. views/footer
 *
 * @author 		Artbees
 * @package 	jupiter/views
 * @version     5.0.0
 */

global $mk_options;

?>

<div id="sub-footer">
	<div class="<?php echo esc_attr( $view_params['footer_grid_status'] ); ?>">
		<?php if ( !empty( $mk_options['footer_logo'] ) ) {?>
		<div class="mk-footer-logo">
		    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr( bloginfo( 'name' ) ); ?>"><img alt="<?php esc_attr( bloginfo( 'name' ) ); ?>" src="<?php echo esc_url( $mk_options['footer_logo'] ); ?>" /></a>
		</div>
		<?php } ?>
    	<?php if ( has_nav_menu( 'footer-menu' ) ) {
			mk_get_view( 'footer', 'sub-footer-nav' ); 
		} ?>
		<div class="respMenuContainer">
			<?php 
			$menu_name = 'sub menu';
			if ( isset( $menu_name ) ) {
				$menu = wp_get_nav_menu_object( $menu_name );
				
				$menu_items = wp_get_nav_menu_items($menu->term_id);
				$menu_list = '<div class="menuContent"><ul class="menuList">';
				
				foreach ( (array) $menu_items as $key => $menu_item ) {
					$title = $menu_item->title;
					$url = $menu_item->url;
					$menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
				}
				$menu_list .= '</ul></div>';
			} else {
				$menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
			}
			echo $menu_list;
			?>
		</div>	
		<div class="mk-footer-disclaimer"><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Disclaimer' ) ) ); ?>"><?php esc_html_e( 'Disclaimer', 'textdomain' ); ?></a></div>			
    	<span class="mk-footer-copyright"><?php echo stripslashes($mk_options['copyright']); ?></span>
	</div>
	<div class="clearboth"></div>
</div>
