<?php
/*
** single.php
** mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
** mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/

get_header();

Mk_Static_Files::addAssets('mk_blog');

$blog_style = 'blog-style-'.mk_get_blog_single_style();
$blog_type = 'blog-post-type-'.mk_get_blog_single_type();
$holder_class = $blog_type . ' ' .$blog_style;

wp_reset_postdata();
wp_reset_postdata();
$currentLang = ICL_LANGUAGE_CODE;
if($currentLang == "zh-hant"){
	$menu_name = '服務一覽';
}elseif($currentLang == "zh-hans"){
	$menu_name = '服务一览';
}else{
	$menu_name = 'Service Overview';
}
?>

<div id="theme-page" class="postPage hasLeftSideBar">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
		<?php
		// $postType = get_post_type_object(get_post_type(get_the_ID()));
        // if ($postType) {
        //     $pageName = esc_html($postType->labels->singular_name);
		// }
		// $parentPageId = get_page_by_title($menu_name)->ID;
		// $parentPageTitle = get_the_title($parentPageId);
		// $parentMenuTitle = $wpse16243_title;
		// $parentPageTitle = substr($parentMenuTitle, 0, -4) . "";
		// $parentPageId = get_page_by_title( $parentPageTitle )->ID;
        ?>
        <div class="bannerContainer fullPageStyle">
			<?php 		
				$postType = get_post_type( get_the_ID() );
				if($postType == "medical_centres"){
					dynamic_sidebar( 'Medical Centres' );
				}  
			?>
        </div>
		<div id="servicesOverviewSideBar" class="menuContainer leftSideBar">
			<div class="menuHeader"><span class="headerTitle"><?php echo $menu_name; ?></span></div>
			<?php
			$menu = wp_nav_menu( array(
				'menu' => $menu_name,
				'depth' => 3,	
				'items_wrap' => '<ul class="multiLayerMenu medicalCentresMenu %2$s">%3$s</ul>'
			));
			?>
		</div>
		<div class="postContainer rightContainer">
			<?php
			$postTitle = get_field("full_title");
			if(empty($postTitle)){
				$postTitle = get_the_title();
			}
			$titleItem = '<div class="postHeader"><span class="postTitle">' . $postTitle . '</span></div>';
			echo $titleItem;
			wp_reset_postdata();
			mk_get_view('blog/components', 'blog-single-content');
			// mk_build_main_wrapper( mk_get_view('singular', 'wp-single-leftsidebar', true), false,  $holder_class);
			?>
		</div>
	</div>
</div>
<?php if ( get_post_type() == 'medical_centres' ){?>
<script>
jQuery(function(){
	jQuery("#servicesOverviewSideBar .medicalCentresMenu .menu-item:first-child>a").addClass("actived");
});
</script>
<?php } ?>

<?php
get_footer();

