<?php /* Template Name: leftSideBar Page */

get_header();


Mk_Static_Files::addAssets('mk_button'); 
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');

wp_reset_postdata();
?>

<div id="theme-page" class="staticPage hasLeftSideBar">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
		<?php
		$currentPageId = get_the_ID();
		if (get_post_type($currentPageId) == "revision"){
			$parentPageId = wp_get_post_parent_id(wp_get_post_parent_id( get_the_ID()));			
		} else {
			$parentPageId = wp_get_post_parent_id( get_the_ID() );			
		}
		$parentPageTitle = get_the_title($parentPageId);
		?>
        <div class="bannerContainer fullPageStyle">
			<?php echo do_shortcode(get_post($parentPageId)->post_content); ?>
        </div>
		<div class="menuContainer leftSideBar">
            <div class="menuHeader"><span class="headerTitle"><?php echo $parentPageTitle; ?></span></div>
			<?php
			$menu = wp_nav_menu( array(
				'menu' => $parentPageTitle,
				'depth' => 1,	
				'items_wrap' => '<ul class="singleLayerMenu %2$s">%3$s</ul>'
			));
			?>
		</div>
		<div class="pageContainer rightContainer">
			<?php
			$titleItem = '<div class="postHeader"><span class="postTitle">' . get_the_title() . '</span></div>';
			echo $titleItem;
			wp_reset_postdata();
			the_content();
			// mk_build_main_wrapper( mk_get_view('singular', 'wp-page', true) );
			?>
		</div>
	</div>
</div>

<?php
get_footer();