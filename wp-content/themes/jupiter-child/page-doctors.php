<?php /* Template Name: Find A Doctor Page */

get_header();


Mk_Static_Files::addAssets('mk_button'); 
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');

?>
<div id="theme-page" class="doctorsPage">
    <div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
    <?php
    $currentPageId = get_the_ID();
    if (get_post_type($currentPageId) == "revision"){
        $parentPageId = wp_get_post_parent_id(wp_get_post_parent_id( get_the_ID()));			
    } else {
        $parentPageId = wp_get_post_parent_id( get_the_ID() );			
    }
    $parentPageTitle = get_the_title($parentPageId);
    ?>
    <div class="bannerContainer fullPageStyle">
        <?php echo do_shortcode(get_post($parentPageId)->post_content); ?>
    </div>
	    <div class="postContainer">
            <div class="headerContainer"><span class="title"><?php echo $parentPageTitle; ?></span></div>
            <?php
            $postType = 'doctors';
            $taxName = 'servicecategories';
            $terms = get_terms( $taxName, array(
                'hide_empty' => 0
            ));
            $serviceListArr = array();
            foreach($terms as $key => $term){
                $termName = $term->name;
                $termSlug = $term->slug;
                $termId = $term->term_id;
                $termPriority = get_field("priority", "servicecategories_$termId");
                $serviceListArr[$termPriority] = $termId;
            }
            wp_reset_postdata();
            ksort($serviceListArr);
            $listShow = '';
            $listShow .= '<div class="searchContainer">';
            $listShow .= '<div class="inputContainer"><input type="text" name="search" id="searchInput" placeholder="' . esc_html__( 'Doctor Name', 'textdomain' ) . '"></div>';
            $listShow .= '<div class="servicesListContainer"><select name="serviceSelect" id="serviceSelect">';
            $listShow .= '<option class="serviceName defalut" value="default">' . esc_html__( 'Specialist Services', 'textdomain' ) . '</option>';
            foreach($serviceListArr as &$value){
                if ($value === reset($serviceListArr)){
                    $listShow .= '<option class="serviceName" value="' . $value . '">' . get_cat_name( $value ) . '</option>';
                }else{
                    $listShow .= '<option class="serviceName" value="' . $value . '">' . get_cat_name( $value ) . '</option>';
                }
            }
            $listShow .= '</select>';
            $listShow .= '</div>';
            $listShow .= '<div class="buttonContainer"><a class="searchBtn">' . esc_html__( 'Search', 'textdomain' ) . '</a></div>';
            $listShow .= '</div>';
            echo $listShow;	
            wp_reset_postdata();
            ?>
            <?php if(ICL_LANGUAGE_CODE == "en") {?>
            <div class="caseFilterContainer">
                <div class="title">Alphabetical Search (By Surname)</div>
                <div id="a" class="initial namefilter">A</div>
                <div id="b" class="initial namefilter">B</div>
                <div id="c" class="initial namefilter">C</div>
                <div id="d" class="initial namefilter">D</div>
                <div id="e" class="initial namefilter">E</div>
                <div id="f" class="initial namefilter">F</div>
                <div id="g" class="initial namefilter">G</div>
                <div id="h" class="initial namefilter">H</div>
                <div id="i" class="initial namefilter">I</div>
                <div id="j" class="initial namefilter">J</div>
                <div id="k" class="initial namefilter">K</div>
                <div id="l" class="initial namefilter">L</div>
                <div id="m" class="initial namefilter">M</div>
                <div id="n" class="initial namefilter">N</div>
                <div id="o" class="initial namefilter">O</div>
                <div id="p" class="initial namefilter">P</div>
                <div id="q" class="initial namefilter">Q</div>
                <div id="r" class="initial namefilter">R</div>
                <div id="s" class="initial namefilter">S</div>
                <div id="t" class="initial namefilter">T</div>
                <div id="u" class="initial namefilter">U</div>
                <div id="v" class="initial namefilter">V</div>
                <div id="w" class="initial namefilter">W</div>
                <div id="x" class="initial namefilter">X</div>
                <div id="y" class="initial namefilter">Y</div>
                <div id="z" class="initial namefilter">Z</div>      
             </div>
    
            <?php } else{ }?>
            <div class="clearContiner">
				<span href="#" class="doctorSearchClear"><?php echo esc_html__( 'Clear Search', 'textdomain' ) ?></span>
			</div>
            <div class="resultCountContainer"></div>
            <div class="doctorGroupContainer"></div>
            
        </div>
    </div>
</div>

<?php $args = array(
        'order'=> 'ASC',
        'meta_key' => 'priority',
        'orderby' => 'meta_value_num',
        'post_type' => 'doctors',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'suppress_filters'=>0
    );
    $posts = get_posts($args);
    $doctorArray = array();
    foreach($posts as $post ){
        $terms = get_the_terms( get_the_ID(), "servicecategories" );
        $catArray = array();
        foreach($terms as $term){
            $termName = $term->name;
            $termId = $term->term_id;
            $termPriority = get_field("priority", "servicecategories_$termId");
            $groupLocation = get_field("location", "servicecategories_$termId");
            $groupTel = get_field("tel", "servicecategories_$termId");
            $groupConsultationHours = get_field("consultation_hours", "servicecategories_$termId");
            $catArray[] = array(
                "termName" => $termName,
                "termId" => $termId,
                "termSort" => $termPriority,
                "termLocation" => $groupLocation,
                "termTel" => $groupTel,
                "termConsultationHours" => $groupConsultationHours,
            );
        }
        $positionArray = array();
        $positions = get_field('position');
        if (is_array($positions) || is_object($positions)){
            foreach($positions as $position){
                $positionName = $position["name"];
                $positionArray[] = array(
                    "positionName" => $positionName,
                );
            }
        }
        $titleArray = array();
        $titles = get_field('title');
        if (is_array($titles) || is_object($titles)){
            foreach($titles as $title){
                $titleName = $title["name"];
                $titleArray[] = array(
                    "titleName" => $titleName,
                );
            }
        }
        $qualificationArray = array();
        $qualifications = get_field('qualification');
        if (is_array($qualifications) || is_object($qualifications)){
            foreach($qualifications as $qualification){
                $qualificationName = $qualification["name"];
                $qualificationArray[] = array(
                    "qualificationName" => $qualificationName,
                );
            }
        }
        
        $articleArray = array();
        $articlesPostType = "medical_articles";
        $articlesRargs = array(
            'post_type' => $articlesPostType,
            'order'=> 'ASC',
            'orderby' => 'menu_order',
            'suppress_filters'=>0,
            'meta_query' => array(
                array(
                    'key' => 'doctor_post',
                    'value' =>  strval(get_the_ID()), 
                    'compare' => 'LIKE'
                )
            ),
        );
        $articlesPosts = get_posts($articlesRargs);
        if (is_array($articlesPosts) || is_object($articlesPosts)){
            foreach($articlesPosts as $articlesPost ){
                $articleId = $articlesPost->ID;
                $articleImage = get_field('document', $articleId)['url'];
                $articleArray[] = array(
                    "articleTitle" => get_the_title($articleId),
                    "articleImage" => $articleImage,
                );
            }
        }
        
        $imageUrl = get_field('image')["url"];
        if(!isset($imageUrl) || trim($imageUrl)===''){
            $imageUrl = wp_get_attachment_image( 198, array('200', '266'), "", array( "class" => "img-responsive" ) );
        }

        $doctorArray[] = array(
            "id" => get_the_ID(),
            "imageUrl" => get_field('image')["url"],
            "name" => get_the_title(),
            "positions" => $positionArray,
            "titles" => $titleArray,
            "qualifications" => $qualificationArray,
            "articles" => $articleArray,
            "doctorPriority" => get_field('priority'),
            "categories"=> $catArray,
        );
    }
    wp_reset_postdata();
    $tempDoctorsArray = array();
    $categoryArray = array();
    $finalCatDoctorArray = array();
    foreach ($doctorArray as $key => $doctor) {
        foreach( $doctor['categories'] as $doctorCategory){
            if(!isset($categoryArray[$doctorCategory['termSort']])){
                $categoryArray[$doctorCategory['termSort']]=array();
            }
            if(!isset($tempDoctorsArray[$doctorCategory['termId']])){
                $tempDoctorsArray[$doctorCategory['termId']]=[];
            }
            if (!array_key_exists($doctorCategory['termSort'], $categoryArray)) {
                array_push($categoryArray[$doctorCategory['termSort']], $doctorCategory['termId']);
                // $categoryArray[$doctorCategory['termSort']][$key] = $doctorCategory['termId'];
                // $tempDoctorsArray[$doctorCategory['termId']]=[];
            }
            array_push($tempDoctorsArray[$doctorCategory['termId']], $doctor);
            $finalCatDoctorArray[$doctorCategory['termId']] = array(
                "groupName" => $doctorCategory['termName'],
                "groupLocation" => $doctorCategory['termLocation'],
                "groupTel" => $doctorCategory['termTel'],
                "groupConsultationHours" => $doctorCategory['termConsultationHours'],
                "doctors"=> $tempDoctorsArray[$doctorCategory['termId']]       
            );
        }
    } 
    wp_reset_postdata();
?>

<script type='text/javascript'>
function buildPost(post){
    var output = "<div class='doctorContainer'>";
    output += "<div class='doctorIdentity'>";
    var url = post.imageUrl;
    if(!url){
        output += "<div class='doctorThumbnail'></div>";
    }else{
        output += "<div class='doctorThumbnail'><img src='"+url+"' /></div>";
    }
    if ('<?php echo ICL_LANGUAGE_CODE ?>' == 'en'){
        output += "<div class='doctorName'><?php echo 'Dr. '?>"+post.name+"</div>";
    }else{
        output += "<div class='doctorName'>"+post.name+"<?php echo esc_html__( 'Doctor', 'textdomain' ) ?></div>"; 
    }
    output += "<div class='doctorPositions'>"; 
    for(var i=0; i<post.positions.length; i++){
        output += "<div class='doctorPosition'>"+post.positions[i].positionName+"</div>";
    }
    output += "</div>";
    output += "<div class='doctorTitles'>"; 
    for(var i=0; i<post.titles.length; i++){
        output += "<div class='doctorTitle'>"+post.titles[i].titleName+"</div>";
    }
    output += "</div>";
    output += "</div>";
    output += "<div class='respShowBtn'><div class='respBtn'><?php echo esc_html__( 'Load More', 'textdomain' ) ?></div></div>";
    output += "<div class='doctorQualifications'><div class='qualificationTitle'><?php echo esc_html__( 'Qualifications', 'textdomain' ) ?></div>"; 
    for(var i=0; i<post.qualifications.length; i++){
        output += "<div class='doctorQualification'>"+post.qualifications[i].qualificationName+"</div>";
    }
    output += "</div>";
    var articleNum = post.articles.length;
    if(articleNum < 1){
        output += " ";
    }else{
        output += "<div class='doctorArticles'><div class='articleTitle'><?php echo esc_html__( 'Health Information', 'textdomain' ) ?></div>";
        var limit = 10;
        var first10 = post.articles.slice(0,limit);
        var remaining = post.articles.slice(limit);   
        for(var i=0; i<first10.length; i++){
            output += "<div class='doctorArticle'><a class='postLink' href='"+first10[i].articleImage+"' target='_blank'>" + first10[i].articleTitle + "</a></div>";
        } 
        if(remaining.length){
            output += '<div class="showMore" style="display:none">';
            for(var i=0; i<item.length; i++){
                output += "<div class='doctorArticle'><a class='postLink' href='"+remaining[i].articleImage+"' target='_blank'>" + remaining[i].articleTitle + "</a></div>";
            } 
            output += '</div>';
            output += "<div class='moreArticle'><div class='moreBtn'><?php echo esc_html__( 'Load More', 'textdomain' ) ?></div>";
        }
        output += "</div>";
    }
    output += "</div>";
    return output;
}

jQuery(document).ready(function($) {
    var posts = <?php echo json_encode($finalCatDoctorArray); ?>;
    var department = {};
    var doctors = {};
    for(i in posts){
        var _department=posts[i];
        departmentInfo = {
            id: i,
            groupConsultationHours: _department.groupConsultationHours,
            groupLocation: _department.groupLocation,
            groupName: _department.groupName,
            groupTel: _department.groupTel,
        };
        var _doctors = _department.doctors;
        
        _doctors.forEach(function(_doctor){
            var objs = [_doctor,  doctors[_doctor.id] || {}]
            doctors[_doctor.id] = objs.reduce(function (r, o) {
                Object.keys(o).forEach(function (k) {
                    r[k] = o[k];
                });
                return r;
            }, {});
            if(!doctors[_doctor.id].department){
                doctors[_doctor.id].department={};
                doctors[_doctor.id].department[i]= departmentInfo;
            }else{
                doctors[_doctor.id].department[i]= departmentInfo;
            }
        });
    }
    function onQueryChange(query, catId, filter){
        var departments = {};
        for(doctorId in doctors){
            var _doctor = doctors[doctorId];
            if(_doctor.name.toLowerCase().indexOf(query.toLowerCase()) !== -1){
                if( !!filter && _doctor.name.toLowerCase().charAt(0)!==filter.charAt(0).toLowerCase()){
                    continue;
                }
                if(catId!=='default' && Object.keys(_doctor.department).indexOf(catId) >= 0){
                    if(departments[catId] ===undefined){
                        departments[catId]={
                            info:_doctor.department[catId],
                            doctors: []
                        };
                    }
                    departments[catId].doctors.push(_doctor);
                }else if(catId==='default'){
                    Object.keys(_doctor.department).forEach(function(departmentId){
                        if(!departments[departmentId]){
                            departments[departmentId]={
                                info:_doctor.department[departmentId],
                                doctors: []
                            };
                        }
                        departments[departmentId].doctors.push(_doctor);
                    })
                }
            }
        }
        return departments;
    }

    var defaultCat = "default";
    $("#serviceSelect").val(defaultCat);
    $(".searchBtn").on("click", function() {
        $('.resultCountContainer').html('');
        var query = jQuery('#searchInput').val();
        var catId = jQuery('#serviceSelect').val();        
        let filterClickVal = $('.caseFilterContainer .namefilter.active').text();
        var filter = filterClickVal || '';
        if(catId === "default" && !query && !filter){
            $('.resultCountContainer').html("<p class='content'><?php echo esc_html__( 'Please Input Keyword', 'textdomain' ) ?></p>");
            $('.doctorGroupContainer').empty();
        }else{
            var departments = onQueryChange(query, catId, filter);
            let resultsString='';
            var count=0;
            for(departmentId in departments){
                var department = departments[departmentId];
                var serviceOutput = '<div class="groupContainer" id=' + department.info.id + '>';
                serviceOutput += '<div class="groupName groupInfo">' + department.info.groupName + '</div>';
                serviceOutput += '<table class="groupTable groupInfo"><tr><td class="infoTitle groupInfo"><?php echo esc_html__( 'Location', 'textdomain' ) ?></td><td class="slit">:</td><td class="infoData">' + department.info.groupLocation + '</td></tr>';
                serviceOutput += '<tr><td class="infoTitle groupInfo"><?php echo esc_html__( 'Telephone', 'textdomain' ) ?></td><td class="slit">:</td><td class="infoData">'  + department.info.groupTel + '</td></tr>';
                var conHoursStr = department.info.groupConsultationHours;
                serviceOutput += '<tr><td class="infoTitle groupInfo"><?php echo esc_html__( 'Consultation Hours', 'textdomain' ) ?></td><td class="slit">:</td><td class="infoData">' + conHoursStr.replace(/\//g, ";") + '</td></tr>';
                serviceOutput += '</table></div>';
                department.doctors.sort (function(a,b){
                    a.doctorPriority > b.doctorPriority
                });
                department.doctors.forEach(function(doctor){
                    count++;
                    serviceOutput += buildPost(doctor);
                });
                resultsString += serviceOutput;
            }
            // $('.resultCountContainer').html("<p class='content'><?php echo esc_html__( 'Search Result ', 'textdomain' ) ?>("+count+")</p>");
            $('.doctorGroupContainer').html(resultsString);
            $('.moreArticle .moreBtn').click(function(){
                if($('.showMore').css('display')==='none'){
                    $('.showMore').show();
                }else{
                    $('.showMore').hide();
                }
               
            })
        }
    });
    $('.caseFilterContainer .namefilter').click(function(){
        $('.caseFilterContainer .namefilter').each(function(){
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });
    $('.doctorSearchClear').click(function(){
        $('#searchInput').val('');
        $("#serviceSelect").val('default');
        $('.caseFilterContainer .namefilter').each(function(){
            $(this).removeClass("active");
        });
    });
    jQuery(".doctorsPage").on( "click", ".respBtn", function() {
        jQuery(this).parent().parent().find(".doctorQualifications").slideDown();
        jQuery(this).parent().parent().find(".doctorArticles").slideDown();
        jQuery(this).fadeOut();
    });
});
</script>

<?php
get_footer();