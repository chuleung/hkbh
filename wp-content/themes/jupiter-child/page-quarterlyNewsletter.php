<?php /* Template Name: Quarterly Newsletter Page */

get_header();


Mk_Static_Files::addAssets('mk_button'); 
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');

wp_reset_postdata();
?>

<div id="theme-page" class="staticPage hasLeftSideBar">
	<div class="theme-page-wrapper mk-main-wrapper mk-grid full-layout">
		<?php
		$currentPageId = get_the_ID();
		$currentPageTitle = get_the_title();
		if (get_post_type($currentPageId) == "revision"){
			$parentPageId = wp_get_post_parent_id(wp_get_post_parent_id( get_the_ID()));			
		} else {
			$parentPageId = wp_get_post_parent_id( get_the_ID() );			
		}
		$parentPageTitle = get_the_title($parentPageId);
        ?>
        <div class="bannerContainer fullPageStyle">
			<?php echo do_shortcode(get_post($parentPageId)->post_content); ?>
        </div>
		<div class="menuContainer leftSideBar">
            <div class="menuHeader"><span class="headerTitle"><?php echo $parentPageTitle; ?></span></div>
			<?php
			$menu = wp_nav_menu( array(
				'menu' => $parentPageTitle,
				'depth' => 1,	
				'items_wrap' => '<ul class="singleLayerMenu %2$s">%3$s</ul>'
			));
			?>
		</div>
		<div class="postContainer rightContainer">
			<?php
			$titleItem = '<div class="postHeader"><span class="postTitle">' . get_the_title() . '</span></div>';
			echo $titleItem;
			$postType = 'quarterly_newsletter';
			$taxName = "newsletteryearcategories";
			$terms = get_terms( $taxName, array(
				'hide_empty' => 0
			));
			$yearListArr = array();
			foreach($terms as $key => $term){
				$termSlug = $term->slug;				
				$termName = $term->name;
				$yearListArr[$termSlug] = $termName;
			}
			wp_reset_postdata();
			arsort($yearListArr);
			$listShow = ' ';
			$listShow .= '<div class="yearListContainer"><select name="year" id="yearSelect">';
			foreach($yearListArr as &$value){
				if ($value === reset($yearListArr)){
					$listShow .= '<option class="yearNum" value="' . $value . '">' . $value . '</option>';
				}else{
					$listShow .= '<option class="yearNum" value="' . $value . '">' . $value . '</option>';
				}
			}
			$listShow .= '</select></div>';
			echo $listShow;
			?>
			<div id='quarterlyNewsletterPosts'>
			</div>
		</div>
	</div>
</div>
<?php
		reset($yearListArr);
		$first_key = key($yearListArr);
		$args = array(
			'post_type' => $postType,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'suppress_filters'=>0
		);
		$posts = get_posts( $args );
		$result = array();
		foreach($posts as $post ){
			$term = wp_get_post_terms(get_the_ID(), 'newsletteryearcategories', array("fields" => "names"));
			$year= $term[0];
			if(!isset($result[$year])){
				$result[$year]=[];	
			}
			preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', do_shortcode($post->post_content), $postContent);
			$defaultImageId = 198;
			$featureImage = empty(get_the_post_thumbnail(get_the_ID(),'medium')) ?  wp_get_attachment_image( $defaultImageId, array('212', '300'), "", array( "class" => "img-responsive" ) ) : get_the_post_thumbnail(get_the_ID(),'medium' );
			$result[$year][]=[
				'title'=> get_the_title(),
				'link'=>$postContent[1],
				'serial'=>$postContent[2],
				'image' => $featureImage,
			];
		}
?>

<script type='text/javascript'>
	function buildPost(post){
		var postImg = post.image;
		var postLink = post.link;
		var postTitle = post.title;
		var postSerial = post.serial;
		var output = "<p class='post'>"+postImg+"<a class='postLink' target='_blank'  href='"+postLink+"'>" + postTitle + "(" +postSerial+ ")</a></p>";
		return output;
	}
	jQuery(document).ready(function($) {
		var posts = <?php echo json_encode($result); ?>;
		var keys = [], k, i, len;
		for (k in posts) {
			if (posts.hasOwnProperty(k)) {
				keys.push(k);
			}
		}
		keys.sort();
		len = keys.length;
		for (i = 0; i < len; i++) {
			k = keys[i];
		}
		var defaultYear = parseInt(<?php echo $_GET['yr'];?>) || k;
		$("#yearSelect").val(defaultYear);
		$('#quarterlyNewsletterPosts').html(
			posts[defaultYear].map(buildPost)
		)
		$("#yearSelect").on("change", function() {		
			var value = $(this).val();
			jQuery('#quarterlyNewsletterPosts').html(
				(posts[value] || []).map(buildPost)
			)
		});
	});
</script>
<?php
get_footer();


