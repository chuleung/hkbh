<?php
global $mk_options;

$mk_footer_class = $show_footer = $disable_mobile = $footer_status = '';

$post_id = global_get_post_id();
if($post_id) {
  $show_footer = get_post_meta($post_id, '_template', true );
  $cases = array('no-footer', 'no-header-footer', 'no-header-title-footer', 'no-footer-title');
  $footer_status = in_array($show_footer, $cases);
}

if($mk_options['disable_footer'] == 'false' || ( $footer_status )) {
  $mk_footer_class .= ' mk-footer-disable';
}

if($mk_options['footer_type'] == '2') {
  $mk_footer_class .= ' mk-footer-unfold';
}


$boxed_footer = (isset($mk_options['boxed_footer']) && !empty($mk_options['boxed_footer'])) ? $mk_options['boxed_footer'] : 'true';
$footer_grid_status = ($boxed_footer == 'true') ? ' mk-grid' : ' fullwidth-footer';
$disable_mobile = ($mk_options['footer_disable_mobile'] == 'true' ) ? $mk_footer_class .= ' disable-on-mobile'  :  ' ';

?>

<section id="mk-footer-unfold-spacer"></section>

<div class="floatmenuContainer  <?php echo (get_locale() == 'en_US') ? 'enVersion' : '';?>">
    <?php
        if(get_locale() == "zh_HK"){?>
            <div class="floatButton"><img src="/wp-content/themes/jupiter-child/custom/images/quickSearchTc.png" /></div>
        <?php }elseif(get_locale() == "zh_CN"){?>
            <div class="floatButton"><img src="/wp-content/themes/jupiter-child/custom/images/quickSearchSc.png" /></div>
        <?php }else{ ?>
            <div class="floatButton"><img src="/wp-content/themes/jupiter-child/custom/images/quickSearchEng.png" /></div>
        <?php }
    ?>	
	<?php 
	$menu_name = 'quickSearchMenu';
	if ( isset( $menu_name ) ) {
		$menu = wp_get_nav_menu_object( $menu_name );
		
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		$menu_list = '<div class="floatMenu menuContent"><ul class="menuList">';
		
		foreach ( (array) $menu_items as $key => $menu_item ) {
			$title = $menu_item->title;
			$url = $menu_item->url;
			$menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
		}
		$menu_list .= '</ul></div>';
	} else {
		$menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
	}
	echo $menu_list;
	?>
	</div>

	<div class="clearboth">
</div>

<section id="mk-footer" class="<?php echo $mk_footer_class; ?>" <?php echo get_schema_markup('footer'); ?>>
    <?php if($mk_options['disable_footer'] == 'true' && !$footer_status) : ?>
    <div class="footer-wrapper<?php echo $footer_grid_status;?>">
        <div class="mk-padding-wrapper">
            <?php //mk_get_view('footer', 'widgets'); 
                $footerMenuHeaderArray = array();
                $menu_name = 'primary-menu';
                if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
                    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                    $menu_items = wp_get_nav_menu_items($menu->term_id);
                    foreach ( $menu_items as $key => $menu_item ) {
                        if( $menu_item->menu_item_parent == 0 ) {
                        $title = $menu_item->title;
                        $url = $menu_item->url;
                        $footerMenuHeaderArray[] = array(
                                "title" => $title,
                                "url" => $url,
                            );
                        }
                    }                  
                }
                foreach( $footerMenuHeaderArray as $footerMenuHeader ){
                    $menu_header = $footerMenuHeader["title"];
                    $headerUrl = $footerMenuHeader["url"];
                    $menu_list = null;
                    $menu_list = wp_nav_menu( array(
                        'menu' => $menu_header,
                        'echo' => false,
                        'fallback_cb' => '__return_false'
                    )); 
                    if ( !empty ( $menu_list ) ){
                        $menu_list = wp_nav_menu( array(
                            'menu' => $menu_header,
                            'depth' => 1,
                            'items_wrap' => '<a class="menuHeader" href="' . $headerUrl . '">' . $menu_header . '</a><ul class="subMenu %2$s">%3$s</ul>'
                        )); 
                    }else{
                        echo '<div class="menu-noSubMenu-container"><a class="menuHeader" href="' . $headerUrl . '">' . $menu_header . '</a></div>';
                    }
                }            
                    
            ?>

            <div class="clearboth"></div>
        </div>
    </div>
    <?php endif;?>
    <?php if ( $mk_options['disable_sub_footer'] == 'true' && ! $footer_status ) { 
        mk_get_view( 'footer', 'sub-footer', false, ['footer_grid_status' => $footer_grid_status] ); 
    } ?>
</section>
</div>
<?php 
    global $is_header_shortcode_added;
    
    if ( $mk_options['seondary_header_for_all'] === 'true' || get_header_style() === '3' || $is_header_shortcode_added === '3' ) {
        mk_get_header_view('holders', 'secondary-menu', ['header_shortcode_style' => $is_header_shortcode_added]); 
    }
?>
</div>

<div class="bottom-corner-btns js-bottom-corner-btns">
<?php
    if ( $mk_options['go_to_top'] != 'false' ) { 
        mk_get_view( 'footer', 'navigate-top' );
    }
    
    if ( $mk_options['disable_quick_contact'] != 'false' ) {
        mk_get_view( 'footer', 'quick-contact' );
    }
    
    do_action('add_to_cart_responsive');
?>
</div>


<?php if ( $mk_options['header_search_location'] === 'fullscreen_search' ) { 
    mk_get_header_view('global', 'full-screen-search');
} ?>

<?php if (!empty($mk_options['body_border']) && $mk_options['body_border'] === 'true') { ?>
    <div class="border-body border-body--top"></div>
    <div class="border-body border-body--left border-body--side"></div>
    <div class="border-body border-body--right border-body--side"></div>
    <div class="border-body border-body--bottom"></div>
<?php } ?>

<footer id="mk_page_footer">
    <?php
    
    wp_footer();

    if( (isset($mk_options['pagespeed-optimization']) and $mk_options['pagespeed-optimization'] != 'false')
     or (isset($mk_options['pagespeed-defer-optimization']) and $mk_options['pagespeed-defer-optimization'] != 'false')) {
    ?>
    <script>
        !function(e){var a=window.location,n=a.hash;if(n.length&&n.substring(1).length){var r=e(".vc_row, .mk-main-wrapper-holder, .mk-page-section, #comments"),t=r.filter("#"+n.substring(1));if(!t.length)return;n=n.replace("!loading","");var i=n+"!loading";a.hash=i}}(jQuery);
    </script>
    <?php } else { ?>
    <script>
        // Run this very early after DOM is ready
        (function ($) {
            // Prevent browser native behaviour of jumping to anchor
            // while preserving support for current links (shared across net or internally on page)
            var loc = window.location,
                hash = loc.hash;

            // Detect hashlink and change it's name with !loading appendix
            if(hash.length && hash.substring(1).length) {
                var $topLevelSections = $('.vc_row, .mk-main-wrapper-holder, .mk-page-section, #comments');
                var $section = $topLevelSections.filter( '#' + hash.substring(1) );
                // We smooth scroll only to page section and rows where we define our anchors.
                // This should prevent conflict with third party plugins relying on hash
                if( ! $section.length )  return;
                // Mutate hash for some good reason - crazy jumps of browser. We want really smooth scroll on load
                // Discard loading state if it already exists in url (multiple refresh)
                hash = hash.replace( '!loading', '' );
                var newUrl = hash + '!loading';
                loc.hash = newUrl;
            }
        }(jQuery));
    </script>
    <?php } ?>


    <?php 
    // Asks W3C Total Cache plugin to move all JS and CSS assets to before body closing tag. It will help getting above 90 grade in google page speed.
    if(isset($mk_options['pagespeed-optimization']) and $mk_options['pagespeed-optimization'] != 'false' and defined('W3TC')) {
        echo "<!-- W3TC-include-js-head -->";
        echo "<!-- W3TC-include-css -->";
    }
    ?>

</footer>  
</body>
</html>