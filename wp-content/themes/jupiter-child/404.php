<?php
/*
* 404.php
 mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
 mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/

get_header();

if(isset($_GET['p']) && $_GET['post_type'] == 'revision'){
    $parentPostId = wp_get_post_parent_id((int)$_GET['p']);
    $postType = get_post_type($parentPostId);
    $ori_user_id = get_current_user_id();
    
    $user_id = 1; // Change user for query the post
    $user = get_user_by( 'id', $user_id );
    if( $user ) {
        wp_set_current_user( $user_id, $user->user_login );
        wp_set_auth_cookie( $user_id );
        do_action( 'wp_login', $user->user_login );
    }

    query_posts(
        array(
            'p'=>(int)$_GET['p'],
            'post_type'=>'revision',
            'post_status'=>'any'
            )
    );
    if ($postType == "page" && $templateName = get_page_template_slug($parentPostId)!=""){
        include($templateName);
    } else {
        Mk_Static_Files::addAssets('mk_blog');
        $blog_style = 'blog-style-'.mk_get_blog_single_style();
        $blog_type = 'blog-post-type-'.mk_get_blog_single_type();
        $holder_class = $blog_type . ' ' .$blog_style;    
        mk_build_main_wrapper( mk_get_view('singular', 'wp-single', true), false,  $holder_class);        
    }


    $user = get_user_by( 'id', $ori_user_id ); // change back to original role
    if( $user ) {
        wp_set_current_user( $user_id, $user->user_login );
        wp_set_auth_cookie( $user_id );
        do_action( 'wp_login', $user->user_login );
    }



    // echo do_shortcode(get_post($_GET['p'])->post_content);
}else{
   mk_build_main_wrapper( mk_get_view('templates', 'wp-404', true) );
}

get_footer();