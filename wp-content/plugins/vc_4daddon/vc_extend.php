<?php
/*
Plugin Name: VC Addon for HKBH
Description: Extend Visual Composer for HKBH
Version: 0.0.1
Author: Raven Chan
*/


if (!defined('ABSPATH')) die('-1');

class VCExtendAddonClass {
    function __construct() {
        add_action( 'init', array( $this, 'integrateWithVC' ) );
        add_shortcode( 'hkbn_adward_images', array( $this, 'renderAdwardImages' ) );
    }
 
    public function integrateWithVC() {
        vc_map( array(
            "name" => __("Adward Images", 'vc_extend'),
            "base" => "hkbn_adward_images",
            "class" => "",
            "controls" => "full",
            "category" => __('HKBH', 'js_composer'),
            "params" => array(
              array(
                  "type" => "attach_images",
                  "holder" => "div",
                  "heading" => __("Images", 'vc_extend'),
                  "param_name" => "adward_images",
              ),
            )
        ));
  
    }

    public function renderAdwardImages($atts){
        $output = "";
        $output .= "<table class='adward_img_table'><tr>";
        $adwardImgs = explode(",", $atts["adward_images"]);
        if (is_array($adwardImgs)){
            foreach ($adwardImgs as $key => $x) {
                $link = wp_get_attachment_image_src($x,"full");
                //$link2 = wp_get_attachment_image_src($x,"thumbnail");
                $output .= "<td><a target='_blank' href='".$link[0]."'><img class='adward_imgs' src='".$link[0]."'/></a></td>";
            }
        }
        $output .= "</tr></table>";
        return $output;
    }

}
new VCExtendAddonClass();