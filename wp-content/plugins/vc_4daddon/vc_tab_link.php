<?php
/*
Plugin Name: VC Tab Link for HKBH
Description: Extend Visual Composer for HKBH
Version: 0.0.1
Author: Raven Chan
*/


if (!defined('ABSPATH')) die('-1');

class VCTabLinkClass {
    function __construct() {
        add_action( 'init', array( $this, 'integrateWithVC2' ) );
        add_shortcode( 'redirect_tab_link', array( $this, 'renderLinkHook' ) );
    }
 
    public function integrateWithVC2() {
        vc_map( array(
            "name" => __("Redirect Tab", 'vc_extend'),
            "base" => "redirect_tab_link",
            "class" => "",
            "controls" => "full",
            "category" => __('HKBH', 'js_composer'),
            "params" => array(
              array(
                  "type" => "textfield",
                  "holder" => "div",
                  "heading" => __("Link", 'vc_extend'),
                  "param_name" => "link",
              ),
            )
        ));
    }

    public function renderLinkHook($atts){
        $output = "";
        $output .= "<input type='hidden' class='custom_link_hook' tar='".$atts['link']."'/>";
        return $output;
    }
}
new VCTabLinkClass();