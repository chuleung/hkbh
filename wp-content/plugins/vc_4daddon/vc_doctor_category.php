<?php
/*
Plugin Name: VC Doctor Category for HKBH
Description: Extend Visual Composer for HKBH
Version: 0.0.1
Author: Martin Chu
*/


if (!defined('ABSPATH')) die('-1');

class VCDoctorCatExtendClass {
    function __construct() {
        add_action( 'init', array( $this, 'integrateWithVC' ) );
        add_shortcode( 'hkbn_doctor_category', array( $this, 'renderDoctorPost' ) );
    }
 
    public function integrateWithVC() {
        $categories_array = array( __( 'All Categories', 'js_composer' ) => 'all-categories' );
        $category_list = get_terms( 'servicecategories', array( 'hide_empty' => false ) );
        if ( is_array( $category_list ) && ! empty( $category_list ) ) {
            foreach ( $category_list as $category_details ) {   
                $categories_array[ $category_details->name ] = $category_details->term_id;  
            }
        }
        vc_map( array(
            "name" => __("Doctor Category", 'vc_extend'),
            "base" => "hkbn_doctor_category",
            "class" => "",
            "category" => __('HKBH', 'doctor_category'),
            "params" => array(
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "heading" => __("Cagegory", 'js_composer'),
                    "param_name" => "doctor_category",
                    'value' => $categories_array,     
                ),
            )
        ));
    }

    public function renderDoctorPost($atts){
        $doctorCat = $atts["doctor_category"]; 
        if( $doctorCat != '' ) {
            $args = array(
                'order'=> 'ASC',
                'meta_key' => 'priority',
                'orderby' => 'meta_value_num',
                'post_type' => 'doctors',
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'servicecategories',
                        'field' => 'term_id',
                        'terms' => $doctorCat,
                    )
                ),
                'posts_per_page' => -1,
                'suppress_filters'=>0
            );
            $posts = get_posts($args);
            $output = "";
            $output .= "<div class='doctorsContainer'>";  
            foreach($posts as $post ){           
                $output .= "<div class='doctorContainer'>";      
                $postId = $post->ID;
                $url = get_field('image', $postId)["url"];
                if( is_null($url)  ){
                    $output .= "<div class='doctorThumbnail'></div>";
                }else{
                    $output .= "<div class='doctorThumbnail'><img src='" . $url. "'/></div>";
                }
                $output .= "<div class='doctorIdentity'>";  
                $name = get_the_title($postId);
                if (ICL_LANGUAGE_CODE == "en"){
                    $output .= "<div class='doctorName'>" . "Dr." .$name. "</div>";
                }else{
                    $output .= "<div class='doctorName'>" . $name . esc_html__( 'Doctor', 'textdomain' ) . "</div>"; 
                }
                $output .= "<div class='doctorPositions'>"; 
                $positions = get_field('position', $postId);
                if (is_array($positions) || is_object($positions)){
                    foreach($positions as $position){
                        $positionName = $position["name"];
                        $output .= "<div class='doctorPosition'>". $positionName ."</div>";
                    }
                }
                $output .= "</div>";
                $output .= "<div class='doctorTitles'>"; 
                $titles = get_field('title', $postId);
                if (is_array($titles) || is_object($titles)){
                    foreach($titles as $title){
                        $titleName = $title["name"];
                        $output .= "<div class='doctorTitle'>". $titleName ."</div>";
                    }
                } 
                $output .= "</div>";
                $output .= "</div>";
                $output .= "<div class='doctorQualifications'><div class='qualificationTitle'>" . esc_html__( 'Qualifications', 'textdomain' ) . "</div>";  
                $qualifications = get_field('qualification', $postId);
                if (is_array($qualifications) || is_object($qualifications)){
                    foreach($qualifications as $qualification){
                        $qualificationName = $qualification["name"];
                        $output .= "<div class='doctorQualification'>". $qualificationName ."</div>";
                    }
                }                
                $output .= "</div>";
                $output .= "</div>";
            }
        }
        $output .= "</div>";
        return $output;
    }
}
new VCDoctorCatExtendClass();