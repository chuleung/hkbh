<?php
/*
Plugin Name: VC Article Category for HKBH
Description: Extend Visual Composer for HKBH
Version: 0.0.1
Author: Martin Chu
*/


if (!defined('ABSPATH')) die('-1');

class VCArticleCatExtendClass {
    function __construct() {
        add_action( 'init', array( $this, 'integrateWithVC' ) );
        add_shortcode( 'hkbn_article_category', array( $this, 'renderArticlePost' ) );
    }
 
    public function integrateWithVC() {
        $categories_array = array( __( 'All Categories', 'js_composer' ) => 'all-categories' );
        $category_list = get_terms( 'articlecategories', array( 'hide_empty' => false ) );
        if ( is_array( $category_list ) && ! empty( $category_list ) ) {
            foreach ( $category_list as $category_details ) {   
                $categories_array[ $category_details->name ] = $category_details->term_id;  
            }
        }
        vc_map( array(
            "name" => __("Article Category", 'vc_extend'),
            "base" => "hkbn_article_category",
            "class" => "",
            "category" => __('HKBH', 'doctorarticle_category'),
            "params" => array(
                array(
                    "type" => "dropdown",
                    "holder" => "div",
                    "heading" => __("Article", 'js_composer'),
                    "param_name" => "article_category",
                    'value' => $categories_array,     
                ),
            )
        ));
    }

    public function renderArticlePost($atts){
        $articleCat = $atts["article_category"]; 
        if( $articleCat != '' ) {
            $args = array(
                'order'=> 'ASC',
                'post_type' => 'medical_articles',
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'articlecategories',
                        'field' => 'term_id',
                        'terms' => $articleCat,
                    )
                ),
                'posts_per_page' => -1,
                'suppress_filters'=>0
            );
            $posts = get_posts($args);
            $output = "";
            $output .= "<div class='articlesContainer'><table class='posts'>";  
            foreach($posts as $post ){       
                $output .= "<tr class='post'>";      
                $postId = $post->ID;
                $url = get_field('document', $postId)["url"];
                $name = get_the_title($postId);
                if( is_null($url)  ){
                    $output .= "<td class='postLink'></td>";
                }else{
                    $output .= "<td class='postLink'><a href='" . $url . "' target='_blank'>" . $name . "</a></td>";
                }
                $output .= "</tr>";
            }
            $output .= "</table></div>";
        }
        return $output;
    }
}
new VCArticleCatExtendClass();