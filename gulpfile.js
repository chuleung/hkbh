// Defining base pathes
var basePaths = {
    bower: './bower_components/',
    node: './node_modules/',
    dev: './src/'
};


// browser-sync watched files
// automatically reloads the page when files changed
var browserSyncWatchFiles = [
    './**/*.php',
    './**/*.css',
    './**/*.js'
];


// browser-sync options
// see: https://www.browsersync.io/docs/options/
// var browserSyncOptions = {
//     proxy: "localhost/4d/ferry/",
//     notify: false
// };

var browserSyncOptions = {
    proxy: "localhost:8080/hkbh/",
    notify: false
};

// Defining requirements
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var sftp = require('gulp-sftp');
// var cssnano = require('gulp-cssnano');
// var merge2 = require('merge2');
// var imagemin = require('gulp-imagemin');
// var ignore = require('gulp-ignore');
// var rimraf = require('gulp-rimraf');
// var clone = require('gulp-clone');
// var merge = require('gulp-merge');
// var del = require('del');





// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch-bs', ['browser-sync', 'watch'], function () { });

// Run:
// gulp browser-sync
// Starts browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    browserSync.init(browserSyncWatchFiles, browserSyncOptions);
});


// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task('watch', function () {
    gulp.watch('wp-content/themes/jupiter-child/custom/scss/*.scss', ['sass']);
    // gulp.watch('css/style.css', ['sftp-style']);
    // gulp.watch('js/*.js', ['scripts']);
});

// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function () {
    var stream = gulp.src('wp-content/themes/jupiter-child/custom/scss/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('wp-content/themes/jupiter-child/custom/css'))
    return stream;
});

// gulp.task('minify-css' , function() {
//   return gulp.src('./css/style.css')
//     .pipe(cleanCSS({
//         compatibility: 'ie8',
//         format: 'keep-breaks'
//     }))
//     .pipe(plumber())
//     .pipe(rename({suffix: '.min'}))
//     .pipe(gulp.dest('./css/'));
// });

// Run:
// gulp scripts.
// Uglifies and concat all JS files into one
// gulp.task('scripts', function() {
//   return gulp.src('./js/*.js')
//     .pipe(concat('theme.min.js'))
//     .pipe(uglify())
//     .pipe(gulp.dest('./js/'));
// });





// Run:
// gulp imagemin
// Running image optimizing task
// gulp.task('imagemin', function(){
//     gulp.src('images/**')
//     .pipe(imagemin())
//     .pipe(gulp.dest('images'))
// });







//---------------------------------------------------------------------------------------







// // Run:
// // gulp sass + cssnano + rename
// // Prepare the min.css for production (with 2 pipes to be sure that "style.css" == "theme.min.css")
// gulp.task('scss-for-prod', function() {
//     var source =  gulp.src('./sass/*.scss')
//         .pipe(plumber())
//         .pipe(sourcemaps.init({loadMaps: true}))
//         .pipe(sass());

//     var pipe1 = source.pipe(clone())
//         .pipe(sourcemaps.write(undefined, { sourceRoot: null }))
//         .pipe(gulp.dest('./css'))
//         .pipe(rename('custom-editor-style.css'))


//     var pipe2 = source.pipe(clone())
//         .pipe(minify-css())
//         .pipe(rename({suffix: '.min'}))
//         .pipe(gulp.dest('./css'));

//     return merge(pipe1, pipe2);
// });


// // Run:
// // gulp sourcemaps + sass + reload(browserSync)
// // Prepare the child-style.css for the development environment
// gulp.task('scss-for-dev', function() {
//     gulp.src('./sass/*.scss')
//         .pipe(plumber())
//         .pipe(sourcemaps.init({loadMaps: true}))
//         .pipe(sass())
//         .pipe(sourcemaps.write(undefined, { sourceRoot: null }))
//         .pipe(gulp.dest('./css'))
// });

// gulp.task('watch-scss', ['browser-sync'], function () {
//     gulp.watch('./sass/**/*.scss', ['scss-for-dev']);
// });



// // Run:
// // gulp cssnano
// // Minifies CSS files
// gulp.task('cssnano', function(){
//   return gulp.src('./css/style.css')
//     .pipe(sourcemaps.init({loadMaps: true}))
//     .pipe(plumber())
//     .pipe(rename({suffix: '.min'}))
//     .pipe(cssnano({discardComments: {removeAll: true}}))
//     .pipe(sourcemaps.write('./'))
//     .pipe(gulp.dest('./css/'))
// });



// gulp.task('cleancss', function() {
//   return gulp.src('./css/*.min.css', { read: false }) // much faster
//     .pipe(ignore('style.css'))
//     .pipe(rimraf());
// });


// // Deleting any file inside the /src folder
// gulp.task('clean-source', function () {
//   return del(['src/**/*',]);
// });

// // Run:
// // gulp copy-assets.
// // Copy all needed dependency assets files from bower_component assets to themes /js, /scss and /fonts folder. Run this task after bower install or bower update

// ////////////////// All Bootstrap SASS  Assets /////////////////////////
// gulp.task('copy-assets', ['clean-source'], function() {

// ////////////////// All Bootstrap 4 Assets /////////////////////////
// // Copy all Bootstrap JS files
//     var stream = gulp.src(basePaths.node + 'bootstrap/dist/js/**/*.js')
//        .pipe(gulp.dest(basePaths.dev + '/js/bootstrap4'));


// // Copy all Bootstrap SCSS files
//     gulp.src(basePaths.node + 'bootstrap/scss/**/*.scss')
//        .pipe(gulp.dest(basePaths.dev + '/sass/bootstrap4'));

// ////////////////// End Bootstrap 4 Assets /////////////////////////

// // Copy all Font Awesome Fonts
//     gulp.src(basePaths.node + 'font-awesome/fonts/**/*.{ttf,woff,woff2,eof,svg}')
//         .pipe(gulp.dest('./fonts'));

// // Copy all Font Awesome SCSS files
//     gulp.src(basePaths.node + 'font-awesome/scss/*.scss')
//         .pipe(gulp.dest(basePaths.dev + '/sass/fontawesome'));

// // Copy jQuery
//     gulp.src(basePaths.node + 'jquery/dist/*.js')
//         .pipe(gulp.dest(basePaths.dev + '/js'));

// // _s SCSS files
//     gulp.src(basePaths.node + 'undescores-for-npm/sass/**/*.scss')
//         .pipe(gulp.dest(basePaths.dev + '/sass/underscores'));

// // _s JS files
//     gulp.src(basePaths.node + 'undescores-for-npm/js/*.js')
//         .pipe(gulp.dest(basePaths.dev + '/js'));

// // Copy Tether JS files
//     gulp.src(basePaths.node + 'tether/dist/js/*.js')
//         .pipe(gulp.dest(basePaths.dev + '/js'));

// // Copy Tether CSS files
//     gulp.src(basePaths.node + 'tether/dist/css/*.css')
//         .pipe(gulp.dest(basePaths.dev + '/css'));
//     return stream;
// });


// // Run
// // gulp dist
// // Copies the files to the /dist folder for distributon as simple theme
// gulp.task('dist', ['clean-dist'], function() {
//     gulp.src(['**/*','!bower_components','!bower_components/**','!node_modules','!node_modules/**','!src','!src/**','!dist','!dist/**','!dist-product','!dist-product/**','!sass','!sass/**','!readme.txt','!readme.md','!package.json','!gulpfile.js','!CHANGELOG.md','!.travis.yml','!jshintignore', '!codesniffer.ruleset.xml', '*'])
//     .pipe(gulp.dest('dist/'))
// });

// // Deleting any file inside the /src folder
// gulp.task('clean-dist', function () {
//   return del(['dist/**/*',]);
// });

// // Run
// // gulp dist-product
// // Copies the files to the /dist-prod folder for distributon as theme with all assets
// gulp.task('dist-product', ['clean-dist-product'], function() {
//     gulp.src(['**/*','!bower_components','!bower_components/**','!node_modules','!node_modules/**','!dist','!dist/**','!dist-product','!dist-product/**', '*'])
//     .pipe(gulp.dest('dist-product/'))
// });

// // Deleting any file inside the /src folder
// gulp.task('clean-dist-product', function () {
//   return del(['dist-product/**/*',]);
// });
